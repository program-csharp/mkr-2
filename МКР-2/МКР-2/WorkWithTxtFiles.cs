﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МКР_2
{
    internal class WorkWithTxtFiles
    {
        public static void SaveDataToTxt(List<Picture> pictureList, string fileName)
        {
            StreamWriter SW = new StreamWriter(fileName);
            for(int i = 0; i < pictureList.Count; i++)
            {
                SW.WriteLine(pictureList[i].Code.ToString() + ", " + pictureList[i].Author.ToString() + ", " + pictureList[i].Name.ToString() + ", " + pictureList[i].Price.ToString() + ", " + pictureList[i].Sign.ToString());
            }
            SW.Close();
        }
    }
}
