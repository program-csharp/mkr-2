﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МКР_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.ColumnCount = 1;
            dataGridView1.RowCount = 1;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if((int)numericUpDown1.Value>0)
                dataGridView1.ColumnCount = (int)numericUpDown1.Value;
        }

        private void знайтиДобутокВідємнихЕлементівВектораToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vector vect = new Vector(dataGridView1);
            MessageBox.Show("Добуток від'ємних: " + vect.ProductNegative());
        }

        private void завдання2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormHalereya form2 = new FormHalereya();
            form2.Show();
        }
    }
}
