﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МКР_2
{
    internal class Vector
    {
        double[] data;
        public Vector(DataGridView grid)
        {
            data = readDataGridView(grid);
        }
        public double [] readDataGridView(DataGridView grid)
        {
            int n = grid.ColumnCount;
            double[] data = new double[n];
            for(int i = 0; i < n;i++)
            {
                data[i] = Convert.ToDouble(grid[i,0].Value);
            }
            return data;
        }

        //Знаходимо добуток від'ємниx елемента
        public double ProductNegative()
        {
            double product = 1;
            foreach(var element in data)
            {
                if (element < 0)
                {
                    product *=element;
                }
            }
            return product;
        }
    }
}
