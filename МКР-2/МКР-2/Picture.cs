﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace МКР_2
{
    internal class Picture
    {
        public string Code{ get; set; }
        public string Author{ get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string Sign { get; set; }
        public Picture(int _code, string _author, string _name, int _price, string _sign)
        {
            Code = Convert.ToString(_code);
            Author = _author;
            Name = _name;
            Price = Convert.ToString(_price);
            Sign = _sign;
        }
    }
}
