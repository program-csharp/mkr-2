﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
namespace МКР_2
{

    internal class WorkWithExcel
    {
        public static void SaveDataToExcel(List<Picture> pictureList,string fileName)
        {
            Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            excelApp.SheetsInNewWorkbook = 1;
            Excel.Workbook workBook = excelApp.Workbooks.Add(Type.Missing);
            excelApp.DisplayAlerts = false;
            Excel.Worksheet sheet = (Excel.Worksheet)excelApp.Worksheets.get_Item(1);
            sheet.Name = "Result";

            sheet.Cells[1, 1] = "Код";
            sheet.Cells[1, 2] = "Автор";
            sheet.Cells[1, 3] = "Назва картини";
            sheet.Cells[1, 4] = "Ціна";
            sheet.Cells[1, 5] = "Ознака";
            for (int i = 0; i < pictureList.Count; i++)
            {
                sheet.Cells[i+2, 1] = pictureList[i].Code;
                sheet.Cells[i+2, 2] = pictureList[i].Author;
                sheet.Cells[i+2, 3] = pictureList[i].Name;
                sheet.Cells[i+2, 4] = pictureList[i].Price;
                sheet.Cells[i+2, 5] = pictureList[i].Sign;
            }
            //Форматуємо клітинки
            Excel.Range range1 = sheet.Range[sheet.Cells[1, 1], sheet.Cells[1, 5]];
            range1.Cells.Font.Name = "Arial";
            range1.Cells.Font.Size = 14;
            range1.Cells.Font.Color = ColorTranslator.ToOle(Color.Black);
            range1.Interior.Color = ColorTranslator.ToOle(Color.Green);

            Excel.Range range2 = sheet.Range[sheet.Cells[2, 1], sheet.Cells[pictureList.Count, 5]];
            range2.Cells.Font.Name = "Arial";
            range2.Cells.Font.Size = 10;
            range2.Cells.Font.Color = ColorTranslator.ToOle(Color.Black);

            //Зберігаємо документ
            excelApp.Application.ActiveWorkbook.SaveAs($"{fileName}");
            //Закриваємо книгу
            workBook.Close(true);
            //Виходимо з додатку
            excelApp.Quit();
        }
    }
}
