﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МКР_2
{
    public partial class FormHalereya : Form
    {
        List<Picture> PictureList;
        BindingSource BindSource;
        public FormHalereya()
        {
            InitializeComponent();
        }

        private void FormHalereya_Load(object sender, EventArgs e)
        {
            PictureList = new List<Picture>();
            PictureList.Add(new Picture(1,"Пікассо","Анабель",100000,"картина в експозиції"));
            PictureList.Add(new Picture(2, "Мунк", "Крик", 400000, "картина в експозиції"));
            PictureList.Add(new Picture(3, "Клімт", "Поцілунок", 99999, "картина в запаснику"));
            PictureList.Add(new Picture(4, "ван Гог", "Зоряна ніч", 100900, "картина в запаснику"));
            PictureList.Add(new Picture(5, "да Вінчі", "Мона Ліза", 300000, "картина в експозиції"));
            BindSource = new BindingSource();
            BindSource.DataSource = PictureList;
            dataGridView1.DataSource = BindSource;
            dataGridView1.Columns["Code"].HeaderText = "Код";
            dataGridView1.Columns["Author"].HeaderText = "Прізвище художника";
            dataGridView1.Columns["Name"].HeaderText = "Назва картини";
            dataGridView1.Columns["Price"].HeaderText = "Ціна";
            dataGridView1.Columns["Sign"].HeaderText = "Ознака";
        }

        List<Picture> PictureListFiltered;
        BindingSource BindSourceFiltered;
        private void button1_Click(object sender, EventArgs e)
        {
            string search = textBox1.Text;
            PictureListFiltered = PictureList.FindAll(picture => picture.Code.Contains(search));
            BindSourceFiltered = new BindingSource();
            BindSourceFiltered.DataSource = PictureListFiltered;
            dataGridView2.DataSource = BindSourceFiltered;
        }

        private void сумарнаЦінаУсіхКартинЩоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PictureListFiltered = PictureList.FindAll(picture => picture.Sign.Contains("картина в запаснику"));
            double summary = 0;
            for(int i = 0; i < PictureListFiltered.Count; i++)
            {
                summary += Convert.ToDouble(PictureListFiltered[i].Price);
            }
            MessageBox.Show(summary.ToString());
        }

        private void експортУExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "xls files (.xlsx)|*.xlsx";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                WorkWithExcel.SaveDataToExcel(PictureList,saveFileDialog1.FileName);
            }
        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (.txt)|*.txt";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                WorkWithTxtFiles.SaveDataToTxt(PictureList, saveFileDialog1.FileName);
            }
        }
    }
}
